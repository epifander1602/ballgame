using UnityEngine;
using UnityEngine.SceneManagement;

public class GameReloader : MonoBehaviour
{
    private bool IsReady;
    
    public void Ready()
    {
        IsReady = true;
    }
    
    public void Restart()
    {
        if (IsReady)
            SceneManager.LoadScene(0);
    }
}