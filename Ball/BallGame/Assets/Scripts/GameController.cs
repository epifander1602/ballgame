using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class GameController : MonoBehaviour
{
    [SerializeField] private Bullet BulletPrefab;
    [SerializeField] private Transform BulletStart;
    [SerializeField] private float MissTargetPoint = 10f;
    [SerializeField] private Player Player;
    [SerializeField] private List<Enemy> Enemies;
    [SerializeField] private UnityEvent OnWin;
    [SerializeField] private UnityEvent OnGameOver;

    private bool IsFinished => GetEnemiesInRadius(Player.CurrentRadius).Count == 0;
    private bool IsGameOver => Player.CurrentRadius <= 0;
    
    private void Awake()
    {
        Player.Init(Hit);
    }

    private void Hit(float attackHealth)
    {
        HitResult hitResult = CalculateHit(attackHealth);
        foreach (Enemy enemy in hitResult.HitEnemies) 
            Enemies.Remove(enemy);
        
        if (IsFinished) 
            Player.Block();
        
        Bullet bullet = Instantiate(BulletPrefab, BulletStart.position, Quaternion.identity);
        bullet.SetSize(attackHealth);
        bullet.StartFly(hitResult.FirstHitPoint, () => OnHit(hitResult.HitEnemies));
    }

    private HitResult CalculateHit(float attackHealth)
    {
        float bulletHitRadius = Player.GetRadius(attackHealth);
        List<Enemy> hitEnemies = GetEnemiesInRadius(bulletHitRadius);
        if (hitEnemies.Count == 0)
            return new HitResult(MissTargetPoint);
        return new HitResult(bulletHitRadius, hitEnemies);
    }
    
    private void OnHit(List<Enemy> enemies)
    {
        foreach (Enemy enemy in enemies) 
            enemy.Kill();

        if (IsGameOver)
        {
            Player.Block();
            OnGameOver.Invoke();
        }
        else if (IsFinished)
        {
            Player.Block();
            OnWin.Invoke();
        }
    }
    
    private List<Enemy> GetEnemiesInRadius(float radius)
    {
        return Enemies.FindAll(enemy => enemy.HitPoint.x <= radius);
    }

    private class HitResult
    {
        public readonly float FirstHitPoint;
        public readonly List<Enemy> HitEnemies;
    
        public HitResult(float bulletHitRadius, List<Enemy> hitEnemies)
        {
            FirstHitPoint = hitEnemies.Min(enemy => enemy.transform.position.z);
            HitEnemies = hitEnemies.FindAll(enemy => enemy.HitPoint.y <= FirstHitPoint + bulletHitRadius);
        }
        
        public HitResult(float missPoint)
        {
            FirstHitPoint = missPoint;
            HitEnemies = new List<Enemy>();
        }
    }
}