using System;
using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private float AttackMultiplier = 1f;
    [SerializeField] private float Radius = 0.5f;
    [SerializeField] private InputComponent InputComponent;
    [SerializeField] private float PrepareBulletSpeed = 0.01f;
    [SerializeField] private Transform Body;
    [SerializeField] private Transform Line;
    [SerializeField] private Transform BulletUI;
    [SerializeField] private GameReloader GameReloader;
    
    private float Health = 1f;
    private float AttackHealth;
    private event Action<float> HitEvent;
    private IEnumerator PrepareBulletCoroutine;
    private bool IsBlocked;

    public float CurrentRadius => GetRadius(Health);

    public void Init(Action<float> hit)
    {
        HitEvent = hit;
        InputComponent.OnTouchDown += StartPrepareBullet;
        InputComponent.OnTouchUp += StopPrepareBullet;
    }
    
    public float GetRadius(float health)
    {
        return Radius * health;
    }
    
    public void Block()
    {
        IsBlocked = true;
    }

    private void StartPrepareBullet()
    {
        if (IsBlocked)
        {
            GameReloader.Restart();
            return;
        }
        
        if (Health <= 0)
            return;
        
        PrepareBulletCoroutine = PrepareBullet();
        StartCoroutine(PrepareBulletCoroutine);
    }

    private void StopPrepareBullet()
    {
        if (PrepareBulletCoroutine == null) 
            return;
        
        StopCoroutine(PrepareBulletCoroutine);
        PrepareBulletCoroutine = null;
        HitEvent?.Invoke(AttackHealth * AttackMultiplier);
        AttackHealth = 0;
        UpdateView();
    }

    private IEnumerator PrepareBullet()
    {
        float startValue = Health;
        while (Health > 0)
        {
            Health -= PrepareBulletSpeed;
            AttackHealth = startValue - Health;
            UpdateView();
            yield return null;
        }
        AttackHealth = startValue;
        UpdateView();
    }

    private void UpdateView()
    {
        Body.localScale = Vector3.one * Health;
        Vector3 lineScale = Line.localScale;
        lineScale.x = Health;
        Line.localScale = lineScale;
        BulletUI.localScale = Vector3.one * (AttackHealth * AttackMultiplier);
    }
}