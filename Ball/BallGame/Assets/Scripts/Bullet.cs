using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class Bullet : MonoBehaviour
{
    [SerializeField] private Transform Body;
    [SerializeField] private float Speed = 0.001f;
    [SerializeField] private float DestroyTime = 0.5f;
    [SerializeField] private UnityEvent OnDestroy;

    public void SetSize(float value)
    {
        Body.localScale = Vector3.one * value;
    }
    
    public void StartFly(float targetPoint, Action onCompleted)
    {
        StartCoroutine(Fly(targetPoint, onCompleted));
    }

    private IEnumerator Fly(float targetPoint, Action onCompleted)
    {
        while (transform.position.z < targetPoint)
        {
            transform.position += Vector3.forward * Speed;
            yield return new WaitForFixedUpdate();
        }
        
        onCompleted?.Invoke();
        OnDestroy.Invoke();
        Destroy(gameObject, DestroyTime);
    }
}