using UnityEngine;
using UnityEngine.Events;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float Radius = 0.5f;
    [SerializeField] private float KillTime = 0.5f;
    [SerializeField] private UnityEvent OnKill;
    
    public Vector2 HitPoint { get; private set; }

    public void Kill()
    {
        OnKill.Invoke();
        Destroy(gameObject, KillTime);
    }

    private void Awake()
    {
        float x = Mathf.Abs(transform.position.x) - Radius;
        float z = transform.position.z - Radius;
        HitPoint = new Vector2(x, z);
        GetComponent<Animator>().SetFloat("random", Random.Range(0, 1f));
    }
}