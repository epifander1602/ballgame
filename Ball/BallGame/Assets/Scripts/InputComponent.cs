using System;
using UnityEngine;

public class InputComponent : MonoBehaviour
{
    public event Action OnTouchDown;
    public event Action OnTouchUp;
    
    private bool IsTouched;
        
    private void Update()
    {
        if (Input.touchCount > 0  || Input.anyKey)
        {
            if (IsTouched) 
                return;
                
            OnTouchDown?.Invoke();
            IsTouched = true;
        }
        else
        {
            if (!IsTouched) 
                return;
                
            OnTouchUp?.Invoke();
            IsTouched = false;
        }
    }
}