using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimatorEvent : MonoBehaviour
{
    [SerializeField] private UnityEvent OnCall;
    
    public void Call()
    {
        OnCall.Invoke();
    }
}
